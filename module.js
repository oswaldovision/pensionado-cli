(function () {
  'use strict'

  var module = angular.module('pagareApp', ['ngComponentRouter','angular-loading-bar','ui.utils.masks'])

  module.value('$routerRootComponent', 'appPagare')

  module.component('appAbout', {
    templateUrl: './views/about.html'
  })

  module.run(function ($rootScope) {
    $rootScope.addressPag = ''
  })
}())