(function () {
  'use strict'
  var module = angular.module('pagareApp')

  var fetchPensioners = function ($http, constantApp) {
    var req = {
      method: 'GET',
      url: constantApp.BASE_URL + '/pensionado/getPensionados',
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return $http(req).then(function (response) {
      return response.data
    })
  }

  var fetchLenders = function ($http, constantApp) {
    var req = {
      method: 'GET',
      url: constantApp.BASE_URL + '/crediticia/getOriginadors',
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return $http(req).then(function (response) {
      return response.data
    })
  }

  var fetchOperators = function ($http, constantApp) {
    var req = {
      method: 'GET',
      url: constantApp.BASE_URL + '/crediticia/getOperadors',
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return $http(req).then(function (response) {
      return response.data
    })
  }

  var fetchCompanies = function ($http, constantApp) {
    var req = {
      method: 'GET',
      url: constantApp.BASE_URL + '/empresa/getCompanies',
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return $http(req).then(function (response) {
      return response.data
    })
  }

  var addPagare = function ($http, model, constantApp) {
    var req = {
      method: 'POST',
      url: constantApp.BASE_URL + '/pagare/addPagare',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        pensionado: model.pensioner,
        propietario: model.owner,
        crediticia: model.lender,
        operador: model.operator,
        empresa: model.company
      }
    }
    return $http(req).then(function (response) {
      return response.data
    }).catch(function (error) {
      return error
    })
  }

  var controller = function ($http, $timeout, $location, constantApp, $rootScope) {
    var model = this
    model.pensioner = ''
    model.owner = ''
    model.lender = ''
    model.operator = ''
    model.company = ''

    model.capacity = ''
    model.amount = ''
    model.price = ''
    model.salesValue = ''

    model.message = ''
    model.showError = false
    model.doFade = false
    model.pensioners = []
    model.lenders = []
    model.operators = []
    model.companies = []

    model.$onInit = function () {
      fetchPensioners($http, constantApp).then(function (listPensioners) {
        model.pensioners = listPensioners
      })
      fetchLenders($http, constantApp).then(function (listLenders) {
        model.lenders = listLenders
      })
      fetchOperators($http, constantApp).then(function (listOpers) {
        model.operators = listOpers
      })
      fetchCompanies($http, constantApp).then(function (listComps) {
        model.companies = listComps
      })
    }

    model.create = function () {
      model.owner = model.lender
      addPagare($http, model, constantApp).then(function (result) {
        console.log(result)
        //reset
        model.showError = false
        model.doFade = false

        model.showError = true
        model.message = result

        $rootScope.addressPag = result

        $timeout(function () {
          model.doFade = true
          $location.path('/details')
        }, 2500)

      }).catch(function (error) {
        model.message = error
      })
    }
  }

  module.component('newPagare', {
    templateUrl: './../views/newPagare.html',
    controllerAs: 'model',
    controller: ['$http', '$timeout', '$location', 'constantApp', '$rootScope', controller]
  })

})()