(function () {
  'use strict'
  var module = angular.module('pagareApp')

  var addCapacity = function ($http, model, constantApp) {
    var req = {
      method: 'POST',
      url: constantApp.BASE_URL + '/pagare/setCupoCredito',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        address: model.addressPagare,
        cupoCredito: model.capacity
      }
    }
    return $http(req).then(function (response) {
      return response.data
    }).catch(function (error) {
      return error
    })
  }

  var saveAmount = function ($http, model, constantApp) {
    var req = {
      method: 'POST',
      url: constantApp.BASE_URL + '/pagare/setSaldo',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        address: model.addressPagare,
        saldoAprobado: model.amount
      }
    }
    return $http(req).then(function (response) {
      return response.data
    }).catch(function (error) {
      return error
    })
  }

  var saveValue = function ($http, model, constantApp) {
    var req = {
      method: 'POST',
      url: constantApp.BASE_URL + '/pagare/setPrecioInicial',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        address: model.addressPagare,
        precioInicial: model.valueCredit
      }
    }
    return $http(req).then(function (response) {
      return response.data
    }).catch(function (error) {
      return error
    })
  }

  var controller = function ($http, $timeout, $location, constantApp, $rootScope) {
    var model = this

    model.capacity = ''
    model.amount = ''
    model.valueCredit = ''

    model.resultCapacity = ''
    model.resultAmount = ''
    model.resultPrice = ''

    model.message = ''
    model.showError = false
    model.doFade = false

    model.$onInit = function () {
      model.addressPagare = $rootScope.addressPag
    }

    model.saveCapacity = function () {
      addCapacity($http, model, constantApp).then(function (result) {
        model.resultCapacity = result
      })
    }

    model.saveAmount = function () {
      saveAmount($http, model, constantApp).then(function (result) {
        model.resultAmount = result
      })
    }

    model.savePrice = function () {
      saveValue($http, model, constantApp).then(function (result) {
        model.resultPrice = result
      })
    }

    // model.saveDetails = function () {
    //   addDetails($http, model, constantApp).then(function (result) {
    //     console.log(result)
    //     //reset
    //     model.showError = false
    //     model.doFade = false
    //
    //     model.showError = true
    //     model.message = result
    //
    //     $timeout(function () {
    //       model.doFade = true
    //       $location.path('/about')
    //     }, 2500)
    //
    //   }).catch(function (error) {
    //     model.message = error
    //   })
    // }
  }

  module.component('detailsPagare', {
    templateUrl: './../views/detailsPagare.html',
    controllerAs: 'model',
    controller: ['$http', '$timeout', '$location', 'constantApp','$rootScope',  controller]
  })

})()