(function () {
  'use strict'

  var module = angular.module('pagareApp')

  var controller = function ($rootScope) {
    var ctrl = this

  }
  module.component('appPagare', {
    templateUrl: './../views/app.html',
    controllerAs: 'ctrl',
    controller: ['$rootScope', controller],
    $routeConfig: [
      {path: '/about', component: 'appAbout', name: 'About'},
      {path: '/new', component: 'newPagare', name: 'New'},
      {path: '/details', component: 'detailsPagare', name: 'Details'},
      {path: '/get', component: 'getPagare', name: 'GetPagare'},
      {path: '/**', redirectTo: ['New']}
    ]
  })

})()