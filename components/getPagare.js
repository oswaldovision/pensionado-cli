(function () {
  'use strict'
  var module = angular.module('pagareApp')

  var getPagare = function ($http, model, constantApp) {
    var req = {
      method: 'POST',
      url: constantApp.BASE_URL + '/pagare/getPagare',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        address: model.addressPagare
      }
    }
    return $http(req).then(function (response) {
      return response.data
    }).catch(function (error) {
      return error
    })
  }

  var inversionistas = function ($http, model, constantApp) {
    var req = {
      method: 'GET',
      url: constantApp.BASE_URL + '/inversionista/getInversionistas',
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return $http(req).then(function (response) {
      return response.data
    }).catch(function (error) {
      return error
    })
  }

  var saveFee = function ($http, model, constantApp) {
    var req = {
      method: 'POST',
      url: constantApp.BASE_URL + '/pagare/pagoCuota',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        address: model.addressPagare,
        valorCuota: model.cuota
      }
    }
    return $http(req).then(function (response) {
      return response.data
    }).catch(function (error) {
      return error
    })
  }

  var setSalesValue = function ($http, model, constantApp) {
    var req = {
      method: 'POST',
      url: constantApp.BASE_URL + '/pagare/setPrecioVenta',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        address: model.addressPagare,
        precioVenta: model.salesValue
      }
    }
    return $http(req).then(function (response) {
      return response.data
    }).catch(function (error) {
      return error
    })
  }

  var setOwner = function ($http, model, constantApp) {
    var req = {
      method: 'POST',
      url: constantApp.BASE_URL + '/pagare/setPropietario',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        address: model.addressPagare,
        propietario: model.inversionista
      }
    }
    return $http(req).then(function (response) {
      return response.data
    }).catch(function (error) {
      return error
    })
  }

  var controller = function ($http, $timeout, $location, constantApp, $rootScope) {
    var model = this
    model.pensioner = ''
    model.owner = ''
    model.lender = ''
    model.operator = ''
    model.company = ''

    model.capacity = ''
    model.amount = ''
    model.price = ''
    model.salesValue = ''
    model.cuota = ''
    model.resultFee = ''
    model.resultSale = ''

    model.inversionista = ''
    model.inversionistas = []
    model.message = ''

    model.$onInit = function () {
      model.addressPagare = $rootScope.addressPag
      model.getInit()
      model.getInversionistas()
    }

    model.saveFee = function () {
      saveFee($http, model, constantApp).then(function (result) {
        model.resultFee = result
        model.getInit()
      }).catch(function (error) {
        model.message = error
      })
    }

    model.saveSale = function () {
      setSalesValue($http, model, constantApp).then(function (result) {
      }).catch(function (error) {
        model.message = error
      })

      setOwner($http, model, constantApp).then(function (result) {
        model.resultSale = result
        model.getInit()
      }).catch(function (error) {
        model.message = error
      })
    }

    model.getInversionistas = function () {
      inversionistas($http, model, constantApp).then(function (result) {
        model.inversionistas = result
      })
    }

    model.getInit = function () {
      getPagare($http, model, constantApp).then(function (result) {
        model.pensioner = result.pensionado
        model.owner = result.propietario
        model.lender = result.crediticia
        model.operator = result.operador
        model.company = result.empresa
        model.capacity = result.cupoCredito
        model.amount = result.saldo
        model.price = result.precioInicial
        model.salesValue = result.precioVenta

      }).catch(function (error) {
        model.message = error
      })
    }
  }

  module.component('getPagare', {
    templateUrl: './../views/getPagare.html',
    controllerAs: 'model',
    controller: ['$http', '$timeout', '$location', 'constantApp', '$rootScope', controller]
  })

})()